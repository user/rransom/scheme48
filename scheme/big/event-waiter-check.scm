
(define-test-suite event-waiters-tests)

(define-test-case no-block-if-an-event-already-occurred event-waiters-tests
  (let* ((ew (make-event-waiter))
         (token (event-waiter-token ew)))
    (signal-event! ew)
    (let ((token (wait-for-new-events ew token)))
      (check 1)
      (signal-event! ew)
      (signal-event! ew)
      (wait-for-new-events ew token)
      (check 2))))

(define-test-case all-events-get-handled event-waiters-tests
  (define (check-all-events-get-handled test-pattern)
    (let* ((ew (make-event-waiter))
           (q-in (make-queue))
           (q-out (make-queue)))
      (spawn (lambda ()
               (let loop1 ((token #f))
                 (let loop2 ()
                   (let ((event (maybe-dequeue! q-in)))
                     (if event
                         (begin
                           (enqueue! q-out event)
                           (loop2)))))
                 (loop1 (wait-for-new-events ew token)))))
      (for-each (lambda (x)
                  (if (eq? x 'signal!)
                      (signal-event! ew)
                      (enqueue! q-in x)))
                test-pattern)
      (sleep 250)
      (check (queue->list q-out)
             => (filter (lambda (x) (not (eq? x 'signal!))) test-pattern))))

  (for-each check-all-events-get-handled
           '((signal! 1 2 signal! 3 signal!)
             (1 signal! 2 signal! 3 signal!)
             (1 2 3 signal!))))

